public class Author {
    private String author,address;
    private int age,authorid;

    @Override
    public String toString() {
        return "Author{" +
                "author='" + author + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                ", authorid=" + authorid +
                '}';
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAuthorid() {
        return authorid;
    }

    public void setAuthorid(int authorid) {
        this.authorid = authorid;
    }

    public Author(String author, String address, int age, int authorid) {
        this.author = author;
        this.address = address;
        this.age = age;
        this.authorid = authorid;
    }

    public Author(String author, String address) {
        this.author = author;
        this.address = address;
        this.age = age;
        this.authorid = authorid;
    }

    public Author() {
    }
}
