import java.util.Scanner;

public class Book {
    private String bookname, genre;
    private int bookid, price, authorid;
    private Author author;

    @Override
    public String toString() {
        return "Book{" +
                "bookname='" + bookname + '\'' +
                ", genre='" + genre + '\'' +
                ", bookid=" + bookid +
                ", price=" + price +
                ", authorid=" + authorid +
                ", author=" + author +
                '}';
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getBookid() {
        return bookid;
    }

    public void setBookid(int bookid) {
        this.bookid = bookid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAuthorid() {
        return authorid;
    }

    public void setAuthorid(int authorid) {
        this.authorid = authorid;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Book(String bookname, String genre, int bookid, int price, int authorid) {
        this.bookname = bookname;
        this.genre = genre;
        this.bookid = bookid;
        this.price = price;
        this.authorid = authorid;
    }

    public Book() {
    }


}
