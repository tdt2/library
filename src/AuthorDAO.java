import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class AuthorDAO implements DAO<Author> {

 public static AuthorDAO getInstance(){
     return new AuthorDAO();}




    @Override
    public int update(Author author) {
        try {
            String sql = "Update tacgia Set " +
                    " Author= '" + author.getAuthor() + "' , " +
                    " Age= '" + author.getAge() + "' , " +
                    " address= '" + author.getAddress() +
                    "' " +
                    " where ID =" + author.getAuthorid();
            Connection c = Jdbc.getConnection();

            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public int insert(Author a) {
     try {
         String sql = "Insert into tacgia (Author,age,address,ID)"
                 + " Values ( '" + a.getAuthor() + "' , '" + a.getAge() + "' , '" + a.getAddress() + "' , " + a.getAuthorid()
                 + " )";
         Connection c = Jdbc.getConnection();
         Statement sttm = c.createStatement();
         sttm.executeUpdate(sql);
         Jdbc.closeConnection(c);
     }
     catch (Exception ex){ex.printStackTrace();}
        return 0;
    }

    @Override
    public int delete(Author author) {
        try{
            String sql= "DELETE from tacgia" + " where " + " id= '" + author.getAuthorid() + "'" ;
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        }
        catch(Exception ex){ex.printStackTrace();}
        return 0;
    }

    @Override
    public ArrayList<Author> selectAll() {
        ArrayList<Author> list=new ArrayList<>();
        try{
            String sql="select * from tacgia";
            Connection c= Jdbc.getConnection();
            Statement sttm=c.createStatement();
            ResultSet rs=sttm.executeQuery(sql);
            while(rs.next()){
                Author a=new Author();
                a.setAuthor(rs.getString("author"));
                a.setAge(rs.getInt("age"));
                a.setAddress(rs.getString("address"));
                a.setAuthorid(rs.getInt("id"));
                list.add(a);
            }
            Jdbc.closeConnection(c);
        }
        catch(Exception ex){ex.printStackTrace();}
        return list;
    }

    @Override
    public ArrayList<Author> selectBy() {
        return null;
    }


    public ArrayList<Author> selectBy(String Condition) {

        ArrayList<Author> list=new ArrayList<>();
        try{
            String sql="select * from tacgia where "+Condition;
            Connection c= Jdbc.getConnection();
            Statement sttm=c.createStatement();
            ResultSet rs=sttm.executeQuery(sql);
            while(rs.next()){
                Author a=new Author();
                a.setAuthor(rs.getString("author"));
                a.setAge(rs.getInt("age"));
                a.setAddress(rs.getString("address"));
                a.setAuthorid(rs.getInt("id"));
                list.add(a);
            }
            Jdbc.closeConnection(c);
        }
        catch(Exception ex){ex.printStackTrace();}
        return list;

    }

    @Override
    public boolean clear() {
        try{
            String sql="Delete from tacgia ";
            Connection c=Jdbc.getConnection();
            Statement sttm=c.createStatement();
            sttm.executeUpdate(sql);
        }
        catch (Exception ex){ex.printStackTrace();}
        return true;
    }
}