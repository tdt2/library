import com.mysql.cj.jdbc.JdbcConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class BookDAO implements DAO<Book> {

    public static BookDAO getinstance() {
        return new BookDAO();
    }

    @Override
    public int insert(Book book) {
        try {
            String sql = "Insert into book_name (id,book_name,genre,price,author_id)"
                    + " Values ( '" + book.getBookid() + "' , '" + book.getBookname() + "' , '" + book.getGenre() + "' , " + book.getPrice()
                    + " , '" + book.getAuthorid()
                    + "' )";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(Book book) {
        try {
            String sql = "Update book_name Set " +
                    " book_name= '" + book.getBookname() + "' , " +
                    " genre= '" + book.getGenre() + "' , " +
                    " price= " + book.getPrice() + " , " +
                    " author_id= " + book.getAuthorid() +
                    " where id= " + book.getBookid();
            ;
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Book book) {
        try {
            String sql = "DELETE from book_name" + " where " + " id= " + book.getBookid();
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public ArrayList<Book> selectAll() {
        ArrayList<Book> list = new ArrayList<>();
        try {
            String sql = "select * from book_name";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);
            while (rs.next()) {
                Book b = new Book();
                b.setBookid(rs.getInt("id"));
                b.setBookname(rs.getString("book_name"));
                b.setGenre(rs.getString("genre"));
                b.setPrice(rs.getInt("price"));
                b.setAuthorid(rs.getInt("author_id"));
                list.add(b);
            }
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @Override
    public ArrayList<Book> selectBy() {
        return null;
    }

    @Override
    public boolean clear() {
        try {
            String sql = "Delete from book_name  ";
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            sttm.executeUpdate(sql);
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return true;
    }


    public ArrayList<Book> selectBy(String Condition) {
        ArrayList<Book> list = new ArrayList<>();
        try {
            String sql = "select * from book_name where " + Condition;
            Connection c = Jdbc.getConnection();
            Statement sttm = c.createStatement();
            ResultSet rs = sttm.executeQuery(sql);
            while (rs.next()) {
                Book b = new Book();
                b.setAuthorid(rs.getInt("id"));
                b.setBookname(rs.getString("book_name"));
                b.setGenre(rs.getString("genre"));
                b.setPrice(rs.getInt("price"));
                b.setAuthorid(rs.getInt("author_id"));
                list.add(b);
            }
            Jdbc.closeConnection(c);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;

    }


}